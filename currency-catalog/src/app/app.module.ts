import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ListCurrenciesComponent } from './components/list-currencies/list-currencies.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  declarations: [
    AppComponent,
    ListCurrenciesComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
