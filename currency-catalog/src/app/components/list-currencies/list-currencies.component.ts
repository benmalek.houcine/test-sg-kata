import { Component, OnInit } from '@angular/core';

import { CurrenciesService } from 'src/app/services/currencies/currencies.service';

@Component({
  selector: 'app-list-currencies',
  templateUrl: './list-currencies.component.html',
  styleUrls: ['./list-currencies.component.css']
})
export class ListCurrenciesComponent implements OnInit {
  currencies: any;

  constructor(private currenciesService: CurrenciesService) { }

  ngOnInit() {
    this.currenciesService.getCurrencies().subscribe(res => {
      this.currencies = res;
      console.log(res);
    });
  }

}
